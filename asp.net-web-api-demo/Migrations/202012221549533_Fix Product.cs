namespace asp.net_web_api_demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixProduct : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "Promotion", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Promotion", c => c.Int(nullable: false));
        }
    }
}
