namespace asp.net_web_api_demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixDiscountRate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "DiscountRate", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "DiscountRate", c => c.Single());
        }
    }
}
