namespace asp.net_web_api_demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModifyAt : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "ModifyAt", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "ModifyAt");
        }
    }
}
