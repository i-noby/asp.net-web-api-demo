namespace asp.net_web_api_demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDiscountRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "DiscountRate", c => c.Single());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "DiscountRate");
        }
    }
}
