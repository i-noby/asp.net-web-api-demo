﻿using asp.net_web_api_demo.Models;
using asp.net_web_api_demo.Services;
using System.Web.Http;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Web.Http.Description;

namespace asp.net_web_api_demo.Controllers
{
    [Authorize]
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        [HttpGet]
        [ResponseType(typeof(PageResult<Order>))]
        public IHttpActionResult List([FromUri] PagingQueryModel pagingQuery)
        {
            PageResult<Order> result;
            using (var orderRepository = new OrderRepository())
            {
                orderRepository.QueryOnly();
                result = orderRepository.GetOrders(pagingQuery);
            }
            return Json(result);
        }

        [HttpGet]
        [ResponseType(typeof(Result<Order>))]
        public IHttpActionResult Detail(int id)
        {
            Order order;
            using (var orderRepository = new OrderRepository())
            {
                orderRepository.QueryOnly();
                order = orderRepository.GetOrderById(id, true);
                order.Items = order.OrderDetails.ToList();
            }
            var result = new Result<Order>
            {
                Data = order
            };
            return Json(result);
        }

        [HttpPost]
        public IHttpActionResult Create(OrderViewModel orderViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var orderRepository = new OrderRepository())
            using (var productRepository = new ProductRepository())
            {
                var ids = orderViewModel.OrderDetails.Select(x => x.ProductId);
                var products = productRepository.GetProductsByIds(ids);
                var userId = User.Identity.GetUserId();
                orderRepository.CreateOrder(userId, orderViewModel, products);
            }
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult Modify(int id, OrderViewModel orderViewModel)
        {
            bool isExisting;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var orderRepository = new OrderRepository())
            using (var productRepository = new ProductRepository())
            {
                var ids = orderViewModel.OrderDetails.Select(x => x.ProductId);
                var products = productRepository.GetProductsByIds(ids);
                isExisting = orderRepository.ModifyOrder(id, orderViewModel, products);
            }
            if (!isExisting)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult Remove(int id)
        {
            bool isExisting;
            using (var orderRepository = new OrderRepository())
            {
                isExisting = orderRepository.RemoveOrderById(id);
            }
            if (!isExisting)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}
