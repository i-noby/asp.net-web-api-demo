﻿using asp.net_web_api_demo.Models;
using asp.net_web_api_demo.Services;
using System.Web.Http;
using System.Web.Http.Description;

namespace asp.net_web_api_demo.Controllers
{
    // [Authorize]
    [RoutePrefix("api/Products")]
    public class ProductsController : ApiController
    {
        [HttpGet]
        [ResponseType(typeof(PageResult<Product>))]
        public IHttpActionResult List([FromUri] PagingQueryModel pagingQuery)
        {
            PageResult<Product> result;
            using (var productRepository = new ProductRepository())
            {
                productRepository.QueryOnly();
                result = productRepository.GetProducts(pagingQuery);
            }
            return Json(result);
        }

        [HttpGet]
        [ResponseType(typeof(Result<Product>))]
        public IHttpActionResult Detail(int id)
        {
            Product product;
            using (var productRepository = new ProductRepository())
            {
                productRepository.QueryOnly();
                product = productRepository.GetProductById(id);
            }
            var result = new Result<Product>
            {
                Data = product
            };
            return Json(result);
        }

        [HttpPost]
        public IHttpActionResult Create(ProductViewModel productViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var productRepository = new ProductRepository())
            {
                productRepository.CreateProduct(productViewModel);
            }
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult Modify(int id, ProductViewModel productViewModel)
        {
            bool isExisting;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var productRepository = new ProductRepository())
            {
                isExisting = productRepository.ModifyProduct(id, productViewModel);
            }
            if (!isExisting)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult Remove(int id)
        {
            bool isExisting;
            using (var productRepository = new ProductRepository())
            {
                isExisting = productRepository.RemoveProductById(id);
            }
            if (!isExisting)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}
