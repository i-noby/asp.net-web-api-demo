﻿using asp.net_web_api_demo.Models;
using System;
using System.Diagnostics;
using System.Linq;

namespace asp.net_web_api_demo.Services
{
    public class BaseRepository : IDisposable
    {
        protected readonly ApplicationDbContext db;

        public BaseRepository(ApplicationDbContext db = null)
        {
            if (db == null)
            {
                db = new ApplicationDbContext();
            }
            this.db = db;
#if DEBUG
            db.Database.Log = msg => Debug.WriteLine(msg);
#endif
        }

        /// <summary>
        /// 禁用更改跟踪和代理生成
        /// 如果您只想獲取數據，但不想修改任何內容，則可以關閉更改跟踪和代理創建。這將提高您的性能，並防止延遲加載。
        /// </summary>
        public void QueryOnly()
        {
            db.Configuration.AutoDetectChangesEnabled = false;
            db.Configuration.ProxyCreationEnabled = false;
        }

        public static PageResult<T> GetPageResult<T>(
            PagingQueryModel pagingQueryModel,
            IQueryable<T> query
        )
        {
            var count = query.Count();
            var totalPages = (int)Math.Ceiling(count / (double)pagingQueryModel.PageSize);
            var metadata = new PagingMetadate
            {
                TotalCount = count,
                PageSize = pagingQueryModel.PageSize,
                PageNumber = pagingQueryModel.PageNumber,
                TotalPages = totalPages,
                HasPreviousPage = pagingQueryModel.PageNumber > 1,
                HasNextPage = pagingQueryModel.PageNumber < totalPages
            };
            var data = query
              .Skip((pagingQueryModel.PageNumber - 1) * pagingQueryModel.PageSize)
              .Take(pagingQueryModel.PageSize)
              .ToList();
            var result = new PageResult<T>
            {
                Data = data,
                Metadate = metadata
            };
            return result;
        }

        public void Dispose()
        {
            db.SaveChanges();
            db.Dispose();
        }
    }
}