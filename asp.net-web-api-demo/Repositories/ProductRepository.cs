﻿using asp.net_web_api_demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace asp.net_web_api_demo.Services
{
    public class ProductRepository : BaseRepository
    {
        public PageResult<Product> GetProducts(PagingQueryModel pagingQueryModel)
        {
            var query = db.Products.AsQueryable();
            var orderedQuery = GetOrderedQuery(pagingQueryModel);
            if (pagingQueryModel.Keyword != null)
            {
                orderedQuery = orderedQuery.Where(x => x.ProductName.Contains(pagingQueryModel.Keyword));
            }
            var result = GetPageResult(pagingQueryModel, orderedQuery);
            return result;
        }

        private IQueryable<Product> GetOrderedQuery(PagingQueryModel pagingQueryModel)
        {
            var query = db.Products.AsQueryable();
            var hasOrdered = false;
            if (pagingQueryModel.OrderBy != null)
            {
                var arr = pagingQueryModel.OrderBy.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                var isFirst = true;
                foreach (var item in arr)
                {
                    var isAsc = item.IndexOf(" ") != -1;
                    var field = item.Replace(" ", "").Replace("-", "");
                    switch (field)
                    {
                        case "ProductName":
                            query = isAsc
                               ? (isFirst
                                   ? query.OrderBy(x => x.ProductName)
                                   : (query as IOrderedQueryable<Product>).ThenBy(x => x.ProductName))
                               : (isFirst
                                   ? query.OrderByDescending(x => x.ProductName)
                                   : (query as IOrderedQueryable<Product>).ThenByDescending(x => x.ProductName));
                            isFirst = false;
                            hasOrdered = true;
                            break;
                        case "UnitPrice":
                            query = isAsc
                               ? (isFirst
                                   ? query.OrderBy(x => x.UnitPrice)
                                   : (query as IOrderedQueryable<Product>).ThenBy(x => x.UnitPrice))
                               : (isFirst
                                   ? query.OrderByDescending(x => x.UnitPrice)
                                   : (query as IOrderedQueryable<Product>).ThenByDescending(x => x.UnitPrice));
                            isFirst = false;
                            hasOrdered = true;
                            break;
                        case "Promotion":
                            query = isAsc
                               ? (isFirst
                                   ? query.OrderBy(x => x.Promotion)
                                   : (query as IOrderedQueryable<Product>).ThenBy(x => x.Promotion))
                               : (isFirst
                                   ? query.OrderByDescending(x => x.Promotion)
                                   : (query as IOrderedQueryable<Product>).ThenByDescending(x => x.Promotion));
                            isFirst = false;
                            hasOrdered = true;
                            break;
                        case "CreateAt":
                            query = isAsc
                               ? (isFirst
                                   ? query.OrderBy(x => x.CreateAt)
                                   : (query as IOrderedQueryable<Product>).ThenBy(x => x.CreateAt))
                               : (isFirst
                                   ? query.OrderByDescending(x => x.CreateAt)
                                   : (query as IOrderedQueryable<Product>).ThenByDescending(x => x.CreateAt));
                            isFirst = false;
                            hasOrdered = true;
                            break;
                        case "ModifyAt":
                            query = isAsc
                               ? (isFirst
                                   ? query.OrderBy(x => x.ModifyAt)
                                   : (query as IOrderedQueryable<Product>).ThenBy(x => x.ModifyAt))
                               : (isFirst
                                   ? query.OrderByDescending(x => x.ModifyAt)
                                   : (query as IOrderedQueryable<Product>).ThenByDescending(x => x.ModifyAt));
                            isFirst = false;
                            hasOrdered = true;
                            break;
                    }
                }
            }
            if (!hasOrdered)
            {
                query = query.OrderByDescending(x => x.CreateAt);
            }
            return query;
        }

        public IEnumerable<Product> GetProductsByIds(IEnumerable<int> ids)
        {
            var products = db.Products.Where(x => ids.Contains(x.Id)).ToList();
            return products;
        }

        public Product GetProductById(int id)
        {
            var product = db.Products.SingleOrDefault(x => x.Id == id);
            return product;
        }

        public void CreateProduct(ProductViewModel productViewModel)
        {
            var product = new Product
            {
                ProductName = productViewModel.ProductName,
                UnitPrice = productViewModel.UnitPrice,
                Promotion = productViewModel.Promotion,
                CreateAt = DateTime.Now
            };
            db.Products.Add(product);
        }

        public bool ModifyProduct(int id, ProductViewModel productViewModel)
        {
            var targetProduct = GetProductById(id);
            var isExisting = targetProduct != null;
            if (isExisting)
            {
                db.Entry(targetProduct).CurrentValues.SetValues(productViewModel);
                targetProduct.ModifyAt = DateTime.Now;
            }
            return isExisting;
        }

        public bool RemoveProductById(int id)
        {
            var targetProduct = GetProductById(id);
            var isExisting = targetProduct != null;
            if (isExisting)
            {
                RemoveProduct(targetProduct);
            }
            return isExisting;
        }

        public void RemoveProduct(Product product)
        {
            db.Products.Remove(product);
        }
    }
}
