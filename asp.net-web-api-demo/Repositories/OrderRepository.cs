﻿using asp.net_web_api_demo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace asp.net_web_api_demo.Services
{
    public class OrderRepository : BaseRepository
    {
        public PageResult<Order> GetOrders(PagingQueryModel pagingQueryModel)
        {
            var query = db.Orders
                .Include(x => x.User)
                .AsQueryable();
            var orderedQuery = GetOrderedQuery(pagingQueryModel);
            if (pagingQueryModel.Keyword != null)
            {
                orderedQuery = orderedQuery.Where(x => x.User.Name.Contains(pagingQueryModel.Keyword));
            }
            var result = GetPageResult(pagingQueryModel, orderedQuery);
            return result;
        }

        private IQueryable<Order> GetOrderedQuery(PagingQueryModel pagingQueryModel)
        {
            var query = db.Orders.AsQueryable();
            var hasOrdered = false;
            if (pagingQueryModel.OrderBy != null)
            {
                var arr = pagingQueryModel.OrderBy.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                var isFirst = true;
                foreach (var item in arr)
                {
                    var isAsc = item.IndexOf(" ") != -1;
                    var field = item.Replace(" ", "").Replace("-", "");
                    switch (field)
                    {
                        case "Subtotal":
                            query = isAsc
                               ? (isFirst
                                   ? query.OrderBy(x => x.Subtotal)
                                   : (query as IOrderedQueryable<Order>).ThenBy(x => x.Subtotal))
                               : (isFirst
                                   ? query.OrderByDescending(x => x.Subtotal)
                                   : (query as IOrderedQueryable<Order>).ThenByDescending(x => x.Subtotal));
                            isFirst = false;
                            hasOrdered = true;
                            break;
                        case "CreateAt":
                            query = isAsc
                               ? (isFirst
                                   ? query.OrderBy(x => x.CreateAt)
                                   : (query as IOrderedQueryable<Order>).ThenBy(x => x.CreateAt))
                               : (isFirst
                                   ? query.OrderByDescending(x => x.CreateAt)
                                   : (query as IOrderedQueryable<Order>).ThenByDescending(x => x.CreateAt));
                            isFirst = false;
                            hasOrdered = true;
                            break;
                        case "ModifyAt":
                            query = isAsc
                               ? (isFirst
                                   ? query.OrderBy(x => x.ModifyAt)
                                   : (query as IOrderedQueryable<Order>).ThenBy(x => x.ModifyAt))
                               : (isFirst
                                   ? query.OrderByDescending(x => x.ModifyAt)
                                   : (query as IOrderedQueryable<Order>).ThenByDescending(x => x.ModifyAt));
                            isFirst = false;
                            hasOrdered = true;
                            break;
                    }
                }
            }
            if (!hasOrdered)
            {
                query = query.OrderByDescending(x => x.CreateAt);
            }
            return query;
        }

        public Order GetOrderById(int id, bool include)
        {
            var Order = include
                ? db.Orders
                    .Include(x => x.OrderDetails)
                    .SingleOrDefault(x => x.Id == id)
                : db.Orders
                    .SingleOrDefault(x => x.Id == id);
            return Order;
        }

        public void CreateOrder(string userId, OrderViewModel orderViewModel, IEnumerable<Product> products)
        {
            var order = new Order
            {
                UserId = userId,
                OrderAt = DateTime.Now,
                CreateAt = DateTime.Now
            };
            OrderService.CalculateOrder(products, order, orderViewModel);
            db.Orders.Add(order);
        }

        public bool ModifyOrder(int id, OrderViewModel orderViewModel, IEnumerable<Product> products)
        {
            var targetOrder = GetOrderById(id, true);
            var isExisting = targetOrder != null;
            if (isExisting)
            {
                db.OrderDetails.RemoveRange(targetOrder.OrderDetails);
                OrderService.CalculateOrder(products, targetOrder, orderViewModel);
                targetOrder.ModifyAt = DateTime.Now;
            }
            return isExisting;
        }

        public bool RemoveOrderById(int id)
        {
            var targetOrder = GetOrderById(id, false);
            var isExisting = targetOrder != null;
            if (isExisting)
            {
                RemoveOrder(targetOrder);
            }
            return isExisting;
        }

        public void RemoveOrder(Order order)
        {
            db.Orders.Remove(order);
        }
    }
}
