﻿using asp.net_web_api_demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace asp.net_web_api_demo.Services
{
    public static class OrderService
    {
        public static void CalculateOrder(
            IEnumerable<Product> products,
            Order order,
            OrderViewModel orderViewModel
        )
        {
            if (order.OrderDetails == null)
            {
                order.OrderDetails = new List<OrderDetail>();
            }
            order.Subtotal = 0;
            foreach (var item in orderViewModel.OrderDetails)
            {
                var product = products.First(x => x.Id == item.ProductId);
                var lineTotal = GetLineTotal(product, item);
                var orderDetail = new OrderDetail
                {
                    ProductId = product.Id,
                    Quantity = item.Quantity,
                    UnitPrice = product.UnitPrice,
                    LineTotal = lineTotal
                };
                order.Subtotal += lineTotal;
                order.OrderDetails.Add(orderDetail);
            }
        }

        private static int GetLineTotal(Product product, OrderDetailViewModel item)
        {
            switch (product.Promotion)
            {
                case PromotionType.BuyOneGetOneFree:
                    {
                        var lineTotal = product.UnitPrice * (item.Quantity / 2);
                        if (item.Quantity % 2 == 1)
                        {
                            lineTotal += product.UnitPrice;
                        }
                        return lineTotal;
                    }
                case PromotionType.Discount:
                    {
                        var lineTotal = product.UnitPrice * item.Quantity;
                        var discountedLineTotal = lineTotal * product.DiscountRate.Value;
                        lineTotal = (int)Math.Round(discountedLineTotal, 0);
                        return lineTotal;
                    }
                default:
                    {
                        var lineTotal = product.UnitPrice * item.Quantity;
                        return lineTotal;
                    }
            }
        }
    }
}