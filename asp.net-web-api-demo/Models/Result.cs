﻿using System.Collections.Generic;

namespace asp.net_web_api_demo.Models
{
    public class Result<T>
    {
        public T Data { get; set; }
    }

    public class PageResult<T>
    {
        public IEnumerable<T> Data { get; set; }
        public PagingMetadate Metadate { get; set; }
    }
}