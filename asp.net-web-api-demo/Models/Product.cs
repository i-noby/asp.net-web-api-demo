﻿using System;
using System.ComponentModel.DataAnnotations;

namespace asp.net_web_api_demo.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public string ProductName { get; set; }

        public int UnitPrice { get; set; }

        public PromotionType? Promotion { get; set; }

        public double? DiscountRate { get; set; }

        public DateTime CreateAt { get; set; }

        public DateTime? ModifyAt { get; set; }
    }

    public enum PromotionType
    {
        BuyOneGetOneFree,
        Discount,
    }
}