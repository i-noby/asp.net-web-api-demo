﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace asp.net_web_api_demo.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }

        public int Subtotal { get; set; }

        public DateTime OrderAt { get; set; }

        public DateTime CreateAt { get; set; }

        public DateTime? ModifyAt { get; set; }

        [JsonIgnore]
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [JsonIgnore]
        [ForeignKey("OrderId")]
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        [NotMapped]
        public IEnumerable<OrderDetail> Items { set; get; }
    }

    public class OrderDetail
    {
        [Key]
        public int Id { get; set; }

        public int OrderId { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public int UnitPrice { get; set; }

        public int LineTotal { get; set; }

        [JsonIgnore]
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
    }
}