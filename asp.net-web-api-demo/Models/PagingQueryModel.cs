﻿namespace asp.net_web_api_demo.Models
{
    public class PagingMetadate
    {
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalPages { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
    }

    public class PagingQueryModel : IPagingQueryModel
    {
        private const int maxPageSize = 20;

        private int _pageSize = 10;

        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public int PageNumber { get; set; } = 1;

        public string OrderBy { get; set; }

        public string Keyword { get; set; }
    }

    public interface IPagingQueryModel
    {
        int PageSize { get; set; }
        int PageNumber { get; set; }
        string OrderBy { get; set; }
        string Keyword { get; set; }
    }
}