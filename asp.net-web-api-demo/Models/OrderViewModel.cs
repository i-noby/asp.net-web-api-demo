﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace asp.net_web_api_demo.Models
{
    public class OrderViewModel
    {
        [Required]
        public virtual IEnumerable<OrderDetailViewModel> OrderDetails { get; set; }
    }

    public class OrderDetailViewModel
    {
        [Required]
        public int ProductId { get; set; }

        [Required]
        public int Quantity { get; set; }
    }
}