﻿using System.ComponentModel.DataAnnotations;

namespace asp.net_web_api_demo.Models
{
    public class ProductViewModel
    {
        [Required]
        public string ProductName { get; set; }

        [Required]
        public int UnitPrice { get; set; }

        public PromotionType? Promotion { get; set; }
    }
}