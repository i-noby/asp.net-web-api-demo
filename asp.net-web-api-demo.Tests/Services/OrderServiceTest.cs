﻿using asp.net_web_api_demo.Models;
using asp.net_web_api_demo.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace asp.net_web_api_demo.Tests.Controllers
{
    [TestClass]
    public class OrderServiceTest
    {
        [TestMethod]
        public void TestSubtotalWithoutPromotion()
        {
            // Arrange
            Product[] products = new Product[]
            {
                new Product { Id = 1, UnitPrice = 100 },
                new Product { Id = 2, UnitPrice = 200 },
                new Product { Id = 3, UnitPrice = 300 },
            };
            Order order = new Order();
            OrderViewModel orderViewModel = new OrderViewModel
            {
                OrderDetails = new OrderDetailViewModel[]
                {
                    new OrderDetailViewModel { ProductId = 1, Quantity = 2 },
                    new OrderDetailViewModel { ProductId = 2, Quantity = 3 },
                    new OrderDetailViewModel { ProductId = 3, Quantity = 4 },
                }
            };

            // Act
            OrderService.CalculateOrder(products, order, orderViewModel);

            // Assert
            var expected = (100 * 2) + (200 * 3) + (300 * 4);
            Assert.AreEqual(order.Subtotal, expected);
        }

        [TestMethod]
        public void TestSubtotalWithBuyOneGetOneFree()
        {
            // Arrange
            Product[] products = new Product[]
            {
                new Product { Id = 1, UnitPrice = 100, Promotion = PromotionType.BuyOneGetOneFree },
                new Product { Id = 2, UnitPrice = 200, Promotion = PromotionType.BuyOneGetOneFree },
                new Product { Id = 3, UnitPrice = 300, Promotion = PromotionType.BuyOneGetOneFree },
            };
            Order order = new Order();
            OrderViewModel orderViewModel = new OrderViewModel
            {
                OrderDetails = new OrderDetailViewModel[]
                {
                    new OrderDetailViewModel { ProductId = 1, Quantity = 2 },
                    new OrderDetailViewModel { ProductId = 2, Quantity = 3 },
                    new OrderDetailViewModel { ProductId = 3, Quantity = 4 },
                }
            };

            // Act
            OrderService.CalculateOrder(products, order, orderViewModel);

            // Assert
            var expected = (100 * 1) + (200 * 2) + (300 * 2);
            Assert.AreEqual(order.Subtotal, expected);
        }

        [TestMethod]
        public void TestSubtotalWithDiscount()
        {
            // Arrange
            Product[] products = new Product[]
            {
                new Product { Id = 1, UnitPrice = 100, Promotion = PromotionType.Discount, DiscountRate = 0.9 },
                new Product { Id = 2, UnitPrice = 200, Promotion = PromotionType.Discount, DiscountRate = 0.8 },
                new Product { Id = 3, UnitPrice = 300, Promotion = PromotionType.Discount, DiscountRate = 0.7 },
            };
            Order order = new Order();
            OrderViewModel orderViewModel = new OrderViewModel
            {
                OrderDetails = new OrderDetailViewModel[]
                {
                    new OrderDetailViewModel { ProductId = 1, Quantity = 2 },
                    new OrderDetailViewModel { ProductId = 2, Quantity = 3 },
                    new OrderDetailViewModel { ProductId = 3, Quantity = 4 },
                }
            };

            // Act
            OrderService.CalculateOrder(products, order, orderViewModel);

            // Assert
            var expected =
                (int)Math.Round(100 * 2 * 0.9, 0) +
                (int)Math.Round(200 * 3 * 0.8, 0) +
                (int)Math.Round(300 * 4 * 0.7, 0);
            Assert.AreEqual(order.Subtotal, expected);
        }

        [TestMethod]
        public void TestSubtotalWithCombination()
        {
            // Arrange
            Product[] products = new Product[]
            {
                new Product { Id = 1, UnitPrice = 100 },
                new Product { Id = 2, UnitPrice = 200, Promotion = PromotionType.BuyOneGetOneFree },
                new Product { Id = 3, UnitPrice = 300, Promotion = PromotionType.Discount, DiscountRate = 0.7 },
            };
            Order order = new Order();
            OrderViewModel orderViewModel = new OrderViewModel
            {
                OrderDetails = new OrderDetailViewModel[]
                {
                    new OrderDetailViewModel { ProductId = 1, Quantity = 2 },
                    new OrderDetailViewModel { ProductId = 2, Quantity = 3 },
                    new OrderDetailViewModel { ProductId = 3, Quantity = 4 },
                }
            };

            // Act
            OrderService.CalculateOrder(products, order, orderViewModel);

            // Assert
            var expected =
                (100 * 2) +
                (200 * 2) +
                (int)Math.Round(300 * 4 * 0.7, 0);
            Assert.AreEqual(order.Subtotal, expected);
        }
    }
}
