export function getHttpErrorMessage(errorResult: any) {
  console.log(errorResult)
  if (errorResult.error) {
    if (errorResult.error.message) {
      return errorResult.error.message
    } else if (errorResult.error.error_description) {
      return errorResult.error.error_description
    } else if (errorResult.error.Message) {
      return errorResult.error.Message
    }
  }
  return errorResult.message
}
