import { map } from 'rxjs/operators'
import * as store from 'store'

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private http: HttpClient,
  ) { }

  register(data: {
    Email: string
    Password: string
    ConfirmPassword: string
  }) {
    const url = 'api/Account/Register'
    return this.http.post(url, data)
  }

  login(data: { username: string, password: string }) {
    const url = 'Token'
    const body = new HttpParams()
      .set('username', data.username)
      .set('password', data.password)
      .set('grant_type', 'password')
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    return this.http.post<{ access_token: string }>(url, body.toString(), { headers }).pipe(map(data => {
      store.set('accessToken', data.access_token)
    }))
  }

  logout() {
    const url = 'api/Account/Logout'
    const data = {}
    return this.http.post(url, data).pipe(map(data => {
      store.remove('accessToken')
    }))
  }

  getUserInfo() {
    const url = 'api/Account/UserInfo'
    return this.http.get<{ Email: string }>(url)
  }
}
