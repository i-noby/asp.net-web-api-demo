import * as store from 'store'

import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  get accessToken() {
    const accessToken = store.get('accessToken')
    return accessToken
  }

  get isLoggedIn() {
    return !!this.accessToken
  }
}
