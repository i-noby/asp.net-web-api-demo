import * as store from 'store'

import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class RequestInterceptor implements HttpInterceptor {
  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authHeaders = this.getAuthHeaders()
    const authReq = req.clone({
      setHeaders: authHeaders,
      url: `https://localhost:44396/${req.url}`,
      withCredentials: true,
    })
    return next.handle(authReq)
  }

  getAuthHeaders() {
    const headers: { [key: string]: string } = {
      'Content-Type': 'application/json'
    }
    const accessToken = store.get('accessToken')
    if (accessToken) {
      headers['Authorization'] = `Bearer ${accessToken}`
    }
    return headers
  }
}
