import { Component, OnInit } from '@angular/core'

import { ApiService } from '../../services/api.service'
import { ConfigService } from '../../services/config.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  email = null

  constructor(
    public configService: ConfigService,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    if (this.configService.isLoggedIn) {
      this.apiService.getUserInfo().subscribe(result => {
        this.email = result.Email
      })
    }
  }

  logout() {
    this.apiService.logout().subscribe(result => {
      location.reload()
    })
  }
}
