import Swal from 'sweetalert2'

import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'

import { getHttpErrorMessage } from '../../helpers/error'
import { ApiService } from '../../services/api.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('gordon@i-noby.com', Validators.email),
    password: new FormControl('Q@1qaz2wsx')
  })

  get username() { return this.loginForm.get('username') }
  get password() { return this.loginForm.get('password') }

  constructor(
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login() {
    if (this.loginForm.invalid) {
      return
    }
    this.apiService
      .login(this.loginForm.value)
      .subscribe(
        result => {
          this.router.navigate(['/'])
        },
        errorResult => {
          Swal.fire(getHttpErrorMessage(errorResult))
        }
      )
  }
}
