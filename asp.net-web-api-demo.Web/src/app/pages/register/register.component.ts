import Swal from 'sweetalert2'

import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'

import { getHttpErrorMessage } from '../../helpers/error'
import { ApiService } from '../../services/api.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    Email: new FormControl('foo@i-noby.com', Validators.email),
    Password: new FormControl('Q@1qaz2wsx'),
    ConfirmPassword: new FormControl('Q@1qaz2wsx')
  })

  get Email() { return this.registerForm.get('Email') }
  get Password() { return this.registerForm.get('Password') }
  get ConfirmPassword() { return this.registerForm.get('ConfirmPassword') }

  constructor(
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  register() {
    if (this.registerForm.invalid) {
      return
    }
    this.apiService
      .register(this.registerForm.value)
      .subscribe(
        result => {
          Swal.fire(getHttpErrorMessage('註冊成功'))
        },
        errorResult => {
          Swal.fire(getHttpErrorMessage(errorResult))
        }
      )
  }
}
